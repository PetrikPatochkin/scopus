<?php 

class LP_User {
	
	protected $db;

	function __construct() {
		$this->db = Zend_Registry::get('db');
	}
	
	function profile($id=NULL) {
		if (!Zend_Auth::getInstance()->hasIdentity()) return false;
		if (!$id) $id = Zend_Auth::getInstance()->getIdentity();
		$sql = "SELECT `login`,`role` FROM `users` WHERE `id`=?";
		$user = $this->db->fetchAll($sql,$id);
		return $user[0];
	}
	
	function isAdmin() {
		if (!Zend_Auth::getInstance()->hasIdentity()) return false;
		$id = Zend_Auth::getInstance()->getIdentity();
		
		$authNamespace = new Zend_Session_Namespace('Zend_Auth');
		$sql = "SELECT * FROM `users` WHERE `id`=? AND `role`='admin' AND `password`='".$authNamespace->password."' AND `active`=1";
		
		$users = $this->db->fetchAll($sql,$id);
		if (count($users)==1)
			return true;
		else
			return false;
	}
	
	function isLogin() {
		if (!Zend_Auth::getInstance()->hasIdentity()) return false;
		$id = Zend_Auth::getInstance()->getIdentity();
		$authNamespace = new Zend_Session_Namespace('Zend_Auth');
		$sql = "SELECT * FROM `users` WHERE `id`=? AND `password`='".$authNamespace->password."' AND `active`=1";
		$users = $this->db->fetchAll($sql,$id);
		if (count($users)==1) return true;
		else return false;
	}
	
	function check_rights($contr, $crud) {
		if($this->isAdmin()) return true;
		$user = $this->profile();
		$i=5; //Дефолтное
		switch ($crud) {
			case "C": $i=1;break;// запись
			case "R": $i=2;break;//	чтение
			case "U": $i=3;break;// обновление
			case "D": $i=4;break;// удаление
		}
		$sql = "SELECT * FROM `roles_rights` WHERE `role`='".$user['role']."' AND `area`='$contr' AND SUBSTRING(`CRUD`,$i,1)=1";
		if (count($this->db->fetchAll($sql))==1) return true;
											else return false;
	}
	
	function roles() {
		$sql = "SELECT * FROM `roles_rights` ORDER BY `role`,`area`";
		$result = array();
		$roles = $this->db->fetchAll($sql);
		foreach($roles as $role) {
			$result[$role['role']][$role['area']] = array("C"=>substr($role['CRUD'],0,1),
															"R"=>substr($role['CRUD'],1,1),
															"U"=>substr($role['CRUD'],2,1),
															"D"=>substr($role['CRUD'],3,1),
															);
		}
		return $result;
	}
	
	function change_role($id,$role) {
		$data = array("role"=>$role);
		$this->db->update('users',$data,"id=".$id);
		return 1;
	}
	
	function change_crud($role,$area,$oper,$active) {
		
		switch ($oper) {
			case "C": $i=1;break;// запись
			case "R": $i=2;break;//	чтение
			case "U": $i=3;break;// обновление
			case "D": $i=4;break;// удаление
		}
		$sql = "UPDATE `roles_rights` SET `CRUD`=CONCAT(SUBSTR(`CRUD`,1,".($i-1)."),$active,SUBSTR(`CRUD`,".($i+1).",".(4-$i).")) WHERE `role`='$role' AND `area`='$area'";
		$this->db->exec($sql);
		return 1;
	}
	
	function activate($id) {
		$sql = "UPDATE `users` SET `active`=IF(`active`=0,1,0)WHERE `id`=".$id;
		$act = $this->db->exec($sql);
		$sql = "SELECT `active` FROM `users` WHERE `id`=".$id;
		$act = $this->db->fetchOne($sql);
		return $act;
	}
	
	function login_is_free($login) {
		$sql = "SELECT * FROM `users` WHERE `login`=?";
		$users = $this->db->fetchAll($sql,$login);
		if(count($users)==0)
			return true;
		else
			return false;
	}
	
	function register($name,$pas,$role) {
		$pass = $this->_md5($pas);
		$sql = "INSERT INTO `users`(`login`,`password`,`role`)VALUES('".$name."','$pass','".$role."')";
		if ($this->db->exec($sql))
			return true;
		else
			return false;
	}
	
	function resetpass($id,$pas) {
		$pass = $this->_md5($pas);
		$data = array('password'=>$pass);
		$this->db->update('users',$data,"id=".$id);
		return true;
	}
	
	function _md5($value) {
		$salt = Zend_Registry::get('config')->salt;
		return md5(md5($value).$salt);
	}
	
	function all_users() {
		$sql = "SELECT `id`,`login`,`role`,`active` FROM `users`";
		$users = $this->db->fetchAll($sql);
		return $users;
	}

}