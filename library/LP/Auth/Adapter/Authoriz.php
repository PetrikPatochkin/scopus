<?php

class LP_Auth_Adapter_Authoriz implements Zend_Auth_Adapter_Interface
{
	//массив, содержащий запись об аутентифицированном пользователе
	protected $_resultArray;
	//конструктор
	//принимает имя пользователя и пароль
	public function __construct($login, $password)
	{
		$this->login = $login;
		$this->password = $password;
	}
	//основной метод вутентификации
	//запрашивает базу данных на предмет наличия подходяих данных
	//возвращает экземпляр Zend_Auth_Result с кодом успеха/неудачи
	public function authenticate()
	{
		$db = Zend_Registry::get('db');
		
		$sql = "SELECT * FROM `users` WHERE `login` = ? AND `password` = ? AND `active`=1";
		$result = $db->fetchAll($sql,array($this->login,$this->password));
		if (count($result) == 1) 
		{
			$authNamespace = new Zend_Session_Namespace('Zend_Auth');
			$authNamespace->password = $this->password;
			return new Zend_Auth_Result(
				Zend_Auth_Result::SUCCESS,$result[0]['id'],array('Authentication successfull'));
			
		} else 
		{
			return new Zend_Auth_Result(
				Zend_Auth_Result::FAILURE,null,array('Введенный пароль неверный, либо такого пользователя не существует'));
		}
	}
}