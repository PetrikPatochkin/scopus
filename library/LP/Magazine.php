<?php

class LP_Magazine {

	protected $db;
	
	function __construct() {
		$this->db = Zend_Registry::get('db');
	}
	
	function magazines() {
		$sql = "SELECT `magazines`.`id`,`ref_all`,`ref_eng`,`ref_scopus`,`date_add`,`login` FROM `magazines` LEFT JOIN `users` ON (`users`.`id`=`magazines`.`user_id`)
				ORDER BY `date_add` DESC";
		$res = $this->db->fetchAll($sql);
		return $res;
	}
	
	function read_file_docx($filename){
		$striped_content = '';
		$content = '';
		if(!$filename || !file_exists($filename)) return false;
		$zip = zip_open($filename);
		if (!$zip || is_numeric($zip)) return false;
		while ($zip_entry = zip_read($zip)) {
			if (zip_entry_open($zip, $zip_entry) == FALSE) continue;
			if (zip_entry_name($zip_entry) != "word/document.xml") continue;
			$content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
			zip_entry_close($zip_entry);
		}// end while
		zip_close($zip);
	
		$content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
		$content = str_replace('</w:r></w:p>', "\r\n", $content);
		$striped_content = strip_tags($content);
		return $striped_content;
	}
	
	function analyze($filename) {
		$return_res = array();
		$result = array();
		$content = $this->read_file_docx($filename);
		if($content !== false) {
			$text .= nl2br($content);
		}
		else {
			echo 'Файл не обнаружен';
			exit;
		}
		
		
		//die($text);
		
		preg_match_all("|(.*)<br[ ]?\/?>|U",$text,$matches);

		$new = array();
		$iter = 0;
		foreach ($matches[1] as $row) {
			if (preg_match("#(ЛИТЕРАТУРА)|(ЛІТЕРАТУРА)|(Література)|(REFERENCES)#i",$row)) continue;
			if (preg_match("|^\d{1,4}\. ?[A-ZА-Я]{1}|",$row))
			{
				$iter++;
				$new[$iter] = $row;
			}
		else
			if (preg_match("| $|",$new[$iter])) $new[$iter] .= $row;
			else							    $new[$iter] .= " ".$row;
		}
		
		$result['ref_all'] = count($new);
		
		$new2 = array();
		foreach($new as $row) {
			if (!preg_match("|[А-Яа-я]+|u",$row))
				$new2[] = $row;
		}
		$return_res['alljournal'] = $new2;
		$result['ref_eng'] = count($new2);
		
		$res = array();
		$noresult = array();
		foreach($new2 as $row) {
			if(preg_match("|\/\/\s+(.+?)\.?\s+[—-]|",$row,$match))
			{
				$res[] = $match[1];
				
			}	
			elseif (preg_match("|[—-]\s+(.+?)\.?\s+[—-]|",$row,$match))	
			{		
				$res[] = $match[1];	
			}
			else 
				$noresult[] = $row;
		}
		
		$return_res['noresult'] = $noresult;
		$return_res['alljournal_name'] = $res;
		$scop_cnt = 0;
		
		foreach ($res as &$row) {
			$sql = "SELECT * FROM `scopus` WHERE `name` = '".trim(addslashes($row))."'";
			$rows = $this->db->fetchAll($sql);
			if (count($rows)>0) {
				$return_res['alljournalscopus_name'][] = trim($row);
				$scop_cnt++;
			}
		}
		$result['ref_scopus'] = $scop_cnt;
		
		$data = array("user_id"=>Zend_Auth::getInstance()->getIdentity(),
					  "file"=>$filename,
					  "ref_all"=>$result['ref_all'],
					  "ref_eng"=>$result['ref_eng'],
					  "ref_scopus"=>$result['ref_scopus'],
					);
		$this->db->insert('magazines',$data);
		
		$return_res['counters'] = $result;
		return $return_res;
	}
    
    function delete($id) {
        $sql = "SELECT * FROM `magazines` WHERE `id`=$id LIMIT 0,1";
        $magazine = $this->db->fetchRow($sql);
        
        if (file_exists($magazine['file'])) {
            unlink($magazine['file']);
        }
        
        $this->db->delete("magazines","id=".$id);
    }
}