<?php 

class LP_StatPage {
	
	protected $db;

	function __construct() {
		$this->db = Zend_Registry::get('db');
	}
	
	function pages() {
		$sql = "SELECT `id`,`href`,`lang` FROM `static_page` ORDER BY `href`";
		$pages = $this->db->fetchAll($sql);
		$result = array();
		foreach($pages as $page)
		{
			$result[$page['href']][$page['lang']] = $page['id'];
		}
		return $result;
	}
	
	function page_info($id) {
		$sql = "SELECT * FROM `static_page` WHERE id=?";
		$page = $this->db->fetchAll($sql,$id);
		if ($page)
			return $page[0];
		else
			return false;
	}
	
	function update_page($id, $data) {
		if ($data['show']=='on')
			$data['show']=1;
		else
			$data['show']=0;
		$this->db->update('static_page',$data,"id=$id");
	}
	
	function delete_page($id) {
		$this->db->delete('static_page',"id=$id");
	}
	
	function page_exists($href,$lang) {
		$sql = "SELECT * FROM `static_page` WHERE `href`='$href' AND `lang`='$lang'";
		$res = $this->db->fetchAll($sql);
		if (count($res)==0)
			return false;
		else
			return true;
	}
	
	function add_page($data) {
		if ($this->db->insert('static_page',$data))
			return true;
		else
			return false;
	}
}