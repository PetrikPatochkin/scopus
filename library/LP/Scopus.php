<?php

class LP_Scopus {

	protected $db;

	function __construct() {
		$this->db = Zend_Registry::get('db');
	}
	
	function select_scopus($page_num=0,$search=NULL,$get_count=true) {
        if($get_count) {
            $sql = "SELECT COUNT(*) FROM `scopus` ";
            if($search!==NULL) $sql .= "WHERE `name` LIKE '%".$search."%'";
            $cnt = $this->db->fetchOne($sql);
        } else $cnt = false;
        
        $start_index = $page_num * 50;
		$sql = "SELECT `id`,`name` FROM `scopus` ";
        if($search!=NULL)
             $sql .="WHERE `name` LIKE '%".$search."%' ";
        $sql .= "ORDER BY `name` LIMIT ".$start_index.",50";
        $rows = $this->db->fetchAll($sql);
        
        return array("counter"=>$cnt,"data"=>$rows);
	}
    
    function delete($id){
        $this->db->delete("scopus","id=".$id);
    }
    
    function update($id,$name) {
        $this->db->update("scopus",array("name"=>$name),"id=".$id);
    }
	
	function newupdt($file) {
		require_once 'public/Classes/PHPExcel/IOFactory.php';

		$objPHPExcel = PHPExcel_IOFactory::load($file);
		$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

		$check = array();
		$i=0;
		foreach($sheetData as $key=>$row) {
			if ($row['A']=="") continue;
			if (($sheetData[$key+1]['A']=="")&&(($sheetData[$key-1]['A']=="")||($key<3))) continue;
			preg_match("{(\d{1,4}\.[^(B )]* )?(.*)}",$row['A'],$matches);
			$check[] = $matches[2];
		}
		return $check;
	}
	
	function scop_update($values,$filename) {
		$data = array("user_id"=>Zend_Auth::getInstance()->getIdentity(),
					  "file"=>$filename,
					  "rows_cnt"=>count($values));
		$this->db->insert("scop_updt",$data);
		$upd_id = $this->db->lastInsertId();
		$rows_upd = 0;
		foreach($values as $row) {
			$sql = "SELECT * FROM `scopus` WHERE `name`='".addslashes($row)."'";
			$rows = $this->db->fetchAll($sql);
			if (count($rows)==0) {
				$data = array("name"=>$row,
							  "upd_id"=>$upd_id);
				$this->db->insert("scopus",$data);
				$rows_upd++;
			}
		}
		$data = array("rows_updated"=>$rows_upd);
		$this->db->update("scop_updt",$data,"id=".$upd_id);
		return $rows_upd;
	}
	
	function updates() {
		$sql = "SELECT `scop_updt`.`id`,`rows_updated`,`date`,`users`.`login` FROM `scop_updt` LEFT JOIN `users` ON (`users`.`id`=`scop_updt`.`user_id`)
				ORDER BY `date` DESC";
		$result = $this->db->fetchAll($sql);
		return $result;
	}
	
	function delete_updt($id) {
		$this->db->delete('scopus',"upd_id=".$id);
		$file = $this->db->fetchOne("SELECT `file` FROM `scop_updt` WHERE `id`=".$id);
		unlink($file);
		$this->db->delete('scop_updt',"id=".$id);
	}
	
	function sum($a,$b) {
		return $a+$b;
	}
}