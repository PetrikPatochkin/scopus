<?php

class LP_Menu {
	
	protected $db;
	
	function __construct() {
		$this->db = Zend_Registry::get('db');
	}
	
	// Для создания иерархии меню(дерево)
	function hierarchy() {
		return $this->make_node(0);
	}
	function make_node($id) {
		$sql = "SELECT * FROM `menu` WHERE `father_id`=? ORDER BY `position`";
		$menus = $this->db->fetchAll($sql,$id);
		if ($menus) $hierarchy = array();
		foreach($menus as $menu)
		{
			//$hierarchy[$menu['id']] = $menu;
			//$hierarchy[$menu['id']]['submenu'] = $this->make_node($menu['id']);
			$menu['submenu'] = $this->make_node($menu['id']);
			$hierarchy[] = $menu;
		}
		return $hierarchy;
	}
	// для создание списка меню (список) с уровнями вложенности
	function list_menu($hierarchy) {
		return $this->list_add_menu(array(), $hierarchy, 0);
	}
	function list_add_menu($list, $arr, $level) {
		foreach($arr as $val) {
			$submenu = $val['submenu'];
			unset($val['submenu']);
			$val['level'] = $level+1;
			$list[] = $val;
			if (is_array($submenu)) $list = $this->list_add_menu($list,$submenu,$level+1);
		}
		return $list;
	}
	
	function menu_info($id) {
		$sql = "SELECT * FROM `menu` WHERE id=?";
		$result = $this->db->fetchAll($sql,$id);
		if (count($result)==1)
			return $result[0];
		else
			return false;
	}
	
	function add_menu($data) {
		/*$sql = "INSERT INTO `menu` (`id`, `father_id`, `href`, `name_ru`, `name_ua`, `name_en`, `position`) VALUES(NULL, '".$data['father_id']."', '".$data['href']."', '".$data['name_ru']."', '".$data['name_ua']."', '".$data['name_en']."', (SELECT MAX(m.id) FROM `menu` m)+1 )";
		return $sql;*/
		
		if ($this->db->insert('menu',$data)) {
				$id = $this->db->lastInsertId('menu');
				if ($this->db->update('menu',array('position'=>$id),"id=$id"))
					return true;
				else
					return false;
			}
		else
			return false;
	}
	
	function edit_menu($id,$data) {
		if ($this->db->update('menu',$data,"id=$id"))
			return true;
		else
			return false;
	}
	
	function delete_menu($id) {
		$sql = "SELECT * FROM `menu` WHERE `father_id` = $id";
		$result = $this->db->fetchAll($sql);
		if (count($result)!=0) {
			foreach($result as $son) {
				if (!$this->delete_menu($son['id']))
					return false;
			}
		}
		
		if ($this->db->delete('menu',"id=$id"))
			return true;
		else
			return false;
	}
	
	function swap($id,$direction) {
		$sql = "SELECT `father_id`,`position` FROM `menu` WHERE `id`=?";
		$row = $this->db->fetchRow($sql,$id);
		
		if ($direction=='up') {
			$sql = "SELECT `id`,`position` FROM `menu` WHERE `father_id`=".$row['father_id']." AND `position`<".$row['position']." ORDER BY `position` DESC";
		}
		elseif($direction=='down'){
			$sql = "SELECT `id`,`position` FROM `menu` WHERE `father_id`=".$row['father_id']." AND `position`>".$row['position']." ORDER BY `position`";
			
		}
		else return false;
		
		$row2 = $this->db->fetchRow($sql);
		if ($row2) {
			$this->db->update('menu',array('position'=>$row2['position']),"id=$id");
			$this->db->update('menu',array('position'=>$row['position']),"id=".$row2['id']);
		}
		
		return true;
	}
	
}