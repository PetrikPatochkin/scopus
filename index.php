<?php


defined('SITE_PATH')
    || define('SITE_PATH', realpath(dirname(__FILE__) ));
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(
    'library'.PATH_SEPARATOR
                    .get_include_path()
				);

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/config.ini','production');
Zend_Registry::set('config',$config->config);

$db = new Zend_Db_Adapter_Pdo_Mysql (
							array(
									'host' => $config->database->params->host,
									'username' => $config->database->params->username,
									'password' => $config->database->params->password,
									'dbname' =>$config->database->params->dbname,
									'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8;')
								)
	);

Zend_Registry::set('db',$db);

$application->bootstrap()
            ->run();