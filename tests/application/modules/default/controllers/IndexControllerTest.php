<?php

class IndexControllerTest extends ControllerTestCase
{
	public function testCountZero()
	{
		// Проверка количетва записей в базе при несуществующем поиске
		$scopus = new LP_Scopus;
		$this->assertSame(0, $scopus->select_scopus(0,"NotexistingFile",true) );
	}
	
	public function testAuthentication()
	{
		// Проверка авторизации к меню
		$adapter = new LP_Auth_Adapter_Authoriz(
					"igor", 
					"123123"
					);
		$auth = Zend_Auth::GetInstance();
		$result=$auth->authenticate($adapter);
		$this->assertSame(true, $result->isValid() );
	}
	
	public function testNotAuthentication()
	{
		// Проверка не прохождения авторизации
		$adapter = new LP_Auth_Adapter_Authoriz(
					"igor", 
					"00000000"
					);
		$auth = Zend_Auth::GetInstance();
		$result=$auth->authenticate($adapter);
		$this->assertSame(true, $result->isValid() );
	}
}
