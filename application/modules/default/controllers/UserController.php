<?php

class UserController extends Zend_Controller_Action
{
	protected $user;

    public function init()
    {
		$this->user = new LP_User;
		if (!$this->user->isAdmin()) $this->_redirect('/?mes=1');
    }
	
	public function indexAction()
	{
		$this->view->users = $this->user->all_users();
		$this->view->roles = $this->user->roles();
	}
	
	public function addAction()
	{
		if ($this->getRequest()->isPost())
		{
			$values = $this->getRequest()->getPost();
			if(empty($values['name']))
				$this->view->error = "Заполните никнейм";
			elseif(empty($values['pas1']))
				$this->view->error = "Пароль не может быть пустым";
			elseif($values['pas1']!=$values['pas2'])
				$this->view->error = "Пароли не совпадают";
			elseif(!$this->user->login_is_free($values['name']))
				$this->view->error = "Логин занят";
			else {
				$this->user->register($values['name'],$values['pas1'],$values['role']);
				$this->_redirect('/user');
			}
			$this->view->values['name'] = $values['name'];
		}
	}
	
	public function passAction() {
		$id = $_GET['id'];
		$this->view->user = $this->user->profile($id);
		if ($this->getRequest()->isPost())
		{
			$values = $this->getRequest()->getPost();
			if(empty($values['pas1']))
				$this->view->error = "Пароль не может быть пустым";
			elseif($values['pas1']!=$values['pas2'])
				$this->view->error = "Пароли не совпадают";
			else {
				$this->user->resetpass($id,$values['pas1']);
				$this->_redirect("/user");
			}
		}
	}
	
	public function activateAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		if ($this->getRequest()->isXmlHttpRequest())
		{
			$id = $this->getRequest()->getParam('id');
			$act = $this->user->activate($id);
			echo Zend_Json::encode($act);
		}
	}
	
	public function changeroleAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		if ($this->getRequest()->isXmlHttpRequest())
		{
			$role = $this->getRequest()->getParam('new_role');
			$id = $this->getRequest()->getParam('id');
			$act = $this->user->change_role($id,$role);
			echo Zend_Json::encode($act);
		}
	}
	
	public function changecrudAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		if ($this->getRequest()->isXmlHttpRequest())
		{
			$role = $this->getRequest()->getParam('role');
			$area = $this->getRequest()->getParam('area');
			$oper = $this->getRequest()->getParam('oper');
			$active = $this->getRequest()->getParam('active');
			$act = $this->user->change_crud($role,$area,$oper,$active);
			echo Zend_Json::encode($act);
		}
	}
}