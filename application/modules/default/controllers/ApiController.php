<?php

class ApiController extends Zend_Controller_Action
{
	protected $user;

    public function init()
    {
		// Отключаем вид
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
    }
	
	public function scopusAction(){
		// Получаем параметр поиска
		$search = $this->getRequest()->getParam("search");
		// Берем переменную подключения к БД
		$db = Zend_Registry::get('db');
		$sql  = "SELECT `name` FROM `scopus` ";
		$sql .= "WHERE `name` LIKE '%".$search."%'";
		$sql .= " ORDER BY `name`";
		//die($sql);
		$result = $db->fetchAll($sql);
		echo Zend_Json::encode($result);
	}
    
}