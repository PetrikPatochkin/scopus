<?php

class MagazineController extends Zend_Controller_Action
{
	protected $user;
	protected $magaz;
	
	public function init()
    {
		$this->user = new LP_User;
		if (!$this->user->isLogin()) $this->_redirect('/login');
		
		$this->magaz = new LP_Magazine;
    }
	
	public function addAction()
	{
		if (!$this->user->check_rights("magazine","C")) $this->_redirect('/?mes=1');//checking rights
		if ($this->getRequest()->getPost('uploadFile'))
		{
			$uploaddir = 'public/files/magazines/';
			$name = md5(time()). "." . end(explode(".", $_FILES['file']['name']));
			$uploadfile = $uploaddir . $name;
			if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
				$result = $this->magaz->analyze($uploadfile);
			} else {die("Не вышло");}
			
			$this->view->result = $result['counters'];
			$this->view->all_href = $result['alljournal'];
			$this->view->all_href_name = $result['alljournal_name'];
			$this->view->all_href_scopus = $result['alljournalscopus_name'];
		}
	}
	
	public function indexAction()
	{
		if (!$this->user->check_rights("magazine","R")) $this->_redirect('/?mes=1');//checking rights
		$this->view->magazines = $this->magaz->magazines();
	}
    
    public function delAction() {
        if (!$this->user->check_rights("magazine","D")) $this->_redirect('/?mes=1');//checking rights
        $id = (int)$_GET['id'];
        $this->magaz->delete($id);
        $this->_redirect($this->getRequest()->getHeader('referer'));
    }
}
