<?php

class ScopusController extends Zend_Controller_Action
{
	protected $user;
	protected $scopus;

    public function init()
    {
		$this->user = new LP_User;
		if (!$this->user->isLogin()) $this->_redirect('/login');
		
		$this->scopus = new LP_Scopus;
    }
	
	public function listAction()
	{
		if (!$this->user->check_rights("scopus","R")) $this->_redirect('/?mes=1');//checking rights
         $s = $this->getRequest()->getParam("search");
        if($this->getRequest()->getParam("ajax")) {
            
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            
            echo Zend_Json::encode($this->scopus->select_scopus(0,$s));
            
        } else {
            $data = $this->scopus->select_scopus(0,$s);
            $this->view->scopus = $data['data'];
            $this->view->counter = $data['counter'];            
        }
	}
    
    public function loadpageAction(){
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
        
        if (!$this->user->check_rights("scopus","R")) die();//checking rights
        $page_id   = (int) $_POST['page'];
        $search = $_POST['search'];
        echo Zend_Json::encode($this->scopus->select_scopus($page_id,$search, false));
    }
    
    public function delAction()
    {
        if (!$this->user->check_rights("scopus","D")) $this->_redirect('/?mes=1');//checking rights
        $id = (int)$_GET['id'];
        $this->scopus->delete($id);
        $this->_redirect($this->getRequest()->getHeader('referer'));
    }
    
    public function ajaxeditAction()
    {
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
        
        if (!$this->user->check_rights("scopus","U")) die();//checking rights
        $id   = (int) $_POST['id'];
        $name =       $_POST['text'];
        if((!$id)||(!$name)) {
            echo Zend_Json::encode(array("type"=>false));
        } else {
            $this->scopus->update($id, $name);
            echo Zend_Json::encode(array("type"=>true,"name"=>$name));    
        }
        
    }
    
	
	public function updatesAction()
	{
		if (!$this->user->check_rights("scopus","R")) $this->_redirect('/?mes=1');//checking rights
		$this->view->updates = $this->scopus->updates();
	}
	
	public function delupdAction()
	{
		if (!$this->user->check_rights("scopus","D")) $this->_redirect('/?mes=1');//checking rights
		$id = $_GET['id'];
		$this->scopus->delete_updt($id);
		$this->_redirect($this->getRequest()->getHeader('referer'));
	}
	
	public function addAction()
	{
		if (!$this->user->check_rights("scopus","C")) $this->_redirect('/?mes=1');//checking rights
		if ($this->getRequest()->getPost('uploadFile'))
		{
			//if(file_put_contents(SITE_PATH."/123.txt","igogog")) die("success");
			$uploaddir = SITE_PATH.'/public/files/scopus/';
			$name = md5(time()). "." . end(explode(".", $_FILES['file']['name']));
			$uploadfile = $uploaddir . $name;
			//die($uploadfile." ".$_FILES['file']['tmp_name']);
			if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
				$result = $this->scopus->newupdt($uploadfile);
			} else {die("Не вышло1");}
			
			$rows_upd = $this->scopus->scop_update($result,$uploadfile);
			$this->view->rows_upd = $rows_upd;
			
			$this->view->result = $result;
		}
	}
}