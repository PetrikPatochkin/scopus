<?php

class LoginController extends Zend_Controller_Action
{
	protected $user;

    public function init()
    {
		// определение языка
		$this->config = Zend_Registry::get('config');
		
		//$this->_helper->layout()->disableLayout();
		$this->user = new LP_User;		
    }

    public function indexAction()
    {		
     	// action body
		if ($this->getRequest()->isPost())
			{
			
			$values = $this->getRequest()->getPost();
			$adapter = new LP_Auth_Adapter_Authoriz(
					$values['login'], 
					$this->user->_md5($values['password'])
					);
			//die($this->user->_md5($values['password']));
			$auth = Zend_Auth::GetInstance();
			$result=$auth->authenticate($adapter);
			
			if ($result->isValid())
				{
				// correct
					$this->_redirect('/');
				}
				else
				{
				//none correct
					$this->view->error = "Вход запрещен";
				}
			}
		else
			{
			// if no POST
				$this->view->info = "Для того чтобы войти на сайт заполните пожалуйста следующие поля.";
			}
    }
	
	public function resetpassAction() {
		if (!$this->user->isLogin()) $this->_redirect('/login');
	
		
		if ($this->getRequest()->isPost())
		{
			$values = $this->getRequest()->getPost();
			if(empty($values['pas1']))
				$this->view->error = "Пароль не может быть пустым";
			elseif($values['pas1']!=$values['pas2'])
				$this->view->error = "Пароли не совпадают";
			else {
				$this->user->resetpass(Zend_Auth::getInstance()->getIdentity(),$values['pas1']);
				$this->_redirect("/");
			}
		}
	
	}
	
	public function logoutAction()
	{
		Zend_Auth::getInstance()->clearIdentity();
		$this->_redirect($this->getRequest()->getHeader('referer'));
	}
}