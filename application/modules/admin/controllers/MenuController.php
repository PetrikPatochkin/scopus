<?php

class Admin_MenuController extends Zend_Controller_Action
{
	protected $user;
	protected $menu;

    public function init()
    {
		$this->user = new LP_User;
		if (!$this->user->isAdmin()) $this->_redirect('/admin/login');
		$this->_helper->layout->setLayout("admin");
		
		$this->menu = new LP_Menu;
    }

    public function indexAction()
    {
		$hierarchy = $this->menu->hierarchy();
		$this->view->menu = $this->menu->list_menu($hierarchy);
    }
	
	public function addAction()
	{
		$id = $this->getRequest()->getParam('id');
		if ($id)
			if (!preg_match('/^\d+$/si',$id)) break;
			
		if ($this->getRequest()->isPost())
		{
			$values=$this->getRequest()->getPost();
			if((empty($values['href']))||
			   (empty($values['name_ru']))||
			   (empty($values['name_ua']))||
			   (empty($values['name_en']))
				)
				$this->view->error = "Не заполнены все поля";
			else
			{
				if ($id)
					$values['father_id'] = $id;
				else
					$values['father_id'] = 0;
				if ($this->view->sql = $this->menu->add_menu($values))
					$this->_redirect('/admin/menu');
			}
			$this->view->values = $values;
		}		
	}
	
	public function editAction()
	{
		$id = $this->getRequest()->getParam('id');
		if (!preg_match('/^\d+$/si',$id)) break;
		
		$this->view->values = $this->menu->menu_info($id);
		
		if ($this->getRequest()->isPost())
		{
			$values=$this->getRequest()->getPost();
			if((empty($values['href']))||
			   (empty($values['name_ru']))||
			   (empty($values['name_ua']))||
			   (empty($values['name_en']))
				)
				$this->view->error = "Не заполнены все поля";
			else
			{
				if ($this->menu->edit_menu($id,$values))
					$this->_redirect('/admin/menu');
			}
			$this->view->values = $values;
		}
	}
	
	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id');
		if (!preg_match('/^\d+$/si',$id)) break;
		
		if ($this->menu->delete_menu($id))
			$this->_redirect('/admin/menu');
	}
	
	public function upAction()
	{
		$id = $this->getRequest()->getParam('id');
		if (!preg_match('/^\d+$/si',$id)) break;
		
		if ($this->menu->swap($id, 'up'))
			$this->_redirect('/admin/menu');
	}
	
	public function downAction()
	{
		$id = $this->getRequest()->getParam('id');
		if (!preg_match('/^\d+$/si',$id)) break;
		
		if ($this->menu->swap($id, 'down'))
			$this->_redirect('/admin/menu');
			
	}
}