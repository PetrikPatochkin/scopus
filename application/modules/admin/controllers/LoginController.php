<?php

class Admin_LoginController extends Zend_Controller_Action
{
	protected $user;

    public function init()
    {
		$this->_redirect('/admin123');
		$this->view->error = "123";
		// определение языка
		$this->config = Zend_Registry::get('config');
    	if ($this->getRequest()->getCookie('lang'))
			$this->lang = $this->getRequest()->getCookie('lang');
		else
			$this->lang = $this->config->default_lang;
		$this->_helper->layout->setLayout("admin");
		
		//$this->_helper->layout()->disableLayout();
		$this->user = new LP_User;
		
		$this->feedback = new LP_Feedback;
		$this->view->new_feedback = $this->feedback->new_feedback();
    }

    public function indexAction()
    {	
		
     	// action body
		if ($this->getRequest()->isPost())
			{
			$values = $this->getRequest()->getPost();
			$adapter = new LP_Auth_Adapter_Authoriz(
					$values['login'], 
					$this->user->_md5($values['password'])
					);
			$auth = Zend_Auth::GetInstance();
			$result=$auth->authenticate($adapter);
			
			if ($result->isValid())
				{
				// correct
				if ($this->user->isAdmin())
					$this->_redirect('/admin');
				else
					$this->view->error = "Вы не являетесь администратором";
				}
				else
				{
				//none correct
					$this->view->error = "Не правильный пароль или еще какая-то ересь";
				}
			}
		else
			{
			// if no POST
				$this->view->info = "Для того чтобы войти на сайт заполните пожалуйста следующие поля.";
			}
    }
	
	public function logoutAction()
	{
		Zend_Auth::getInstance()->clearIdentity();
		$this->_redirect($this->getRequest()->getHeader('referer'));
	}
}