<?php

class Admin_CategoryController extends Zend_Controller_Action
{
	protected $user;
	private $catalog;
	
	public function init()
	{
		$this->user = new LP_User;
		if (!$this->user->isAdmin()) $this->_redirect('/admin/login');
		$this->_helper->layout->setLayout("admin");
		$this->db_config = new LP_Config;
		
		$this->catalog = new LP_Catalog;
		
		$this->feedback = new LP_Feedback;
		$this->view->new_feedback = $this->feedback->new_feedback();
	}
	
	public function indexAction()
	{
		$this->view->catalog_level1 = $this->catalog->catalog_level1();
	}
	
	public function viewAction()
	{
		if ($_GET['id']) $id = $_GET['id'];
		else 			 break;
		
		$this->view->categ_info = $this->catalog->catalog_info($id);
		$this->view->categ_characters = $this->catalog->catalog_characters($id);
		$this->view->categ_childs = $this->catalog->catalog_level2($id);
	}
	
	public function characterAction()
	{
		
	}
}
?>