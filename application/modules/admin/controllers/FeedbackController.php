<?

class Admin_FeedbackController extends Zend_Controller_Action
{
	protected $feedback;
	protected $news;

    public function init()
    {
		$this->user = new LP_User;
		if (!$this->user->isAdmin()) $this->_redirect('/admin/login');
		$this->_helper->layout->setLayout("admin");
		
		
		$this->feedback = new LP_Feedback;
    }
	
	public function postDispatch()
	{
		$this->view->new_feedback = $this->feedback->new_feedback();
	}
	
	public function indexAction()
	{
		if ($type=$this->getRequest()->getParam('type')) {
			if (!preg_match("/[a-z]+/",$type)) $this->redirect('/');
			$this->view->feedbacks = $this->feedback->all_feedback($type);
			$this->view->type = $type;
		} elseif($id=$this->getRequest()->getParam('id')) {
			if (!preg_match("/\d+/",$id)) $this->redirect('/');
			$this->view->feed = $this->feedback->feed_info($id);
			$this->feedback->was_read($id);
		} else {
			$this->view->new_question = $this->feedback->new_feedback('question');
			$this->view->new_askcall = $this->feedback->new_feedback('askcall');
			$this->view->new_comment = $this->feedback->new_feedback('comment');
		}
		
	}
}