<?php

class Admin_IndexController extends Zend_Controller_Action
{
	protected $user;

    public function init()
    {
		$this->user = new LP_User;
		if (!$this->user->isAdmin()) $this->_redirect('/admin/login');
		$this->_helper->layout->setLayout("admin");
		
		$this->feedback = new LP_Feedback;
		$this->view->new_feedback = $this->feedback->new_feedback();
    }

    public function indexAction()
    {
    	
		
    }


}

