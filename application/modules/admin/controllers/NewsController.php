<?php

class Admin_NewsController extends Zend_Controller_Action
{
	protected $user;
	protected $news;

    public function init()
    {
		$this->user = new LP_User;
		if (!$this->user->isAdmin()) $this->_redirect('/admin/login');
		$this->_helper->layout->setLayout("admin");
		
		$this->news = new LP_News;
		
		$this->feedback = new LP_Feedback;
		$this->view->new_feedback = $this->feedback->new_feedback();
    }

    public function indexAction()
    {
		$type = $this->getRequest()->getParam('type');
		$this->view->type = $type;
		$this->view->allnews = $this->news->type_news($type);
    }
	
	public function editAction()
	{
		$id = $this->getRequest()->getParam('id');
		if (!preg_match('/^\d+$/si',$id)) break;
		
		if ($this->getRequest()->isPost())
		{
			$values=$this->getRequest()->getPost();
			$values['text'] = stripslashes($values['text']);
			$this->news->update_news($id, $values);
		}
		$this->view->news  = $this->news->news_info($id);
			
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id');
		if (!preg_match('/^\d+$/si',$id)) break;
		
		$this->news->delete_news($id);
		$this->_redirect('/admin/news');
	}
	
	public function addAction()
	{
		$type 	  = $this->getRequest()->getParam('type');
		if (!preg_match('/^[a-z]+$/',$type)) break;
		$group_id = $this->getRequest()->getParam('group_id');
		if (!preg_match('/^\d*$/si',$group_id)) break;
		if (($group_id=="")||(($group_id==0))) $group_id = $this->news->new_group_id();
		
		
		if ($this->getRequest()->isPost())
		{
			$values=$this->getRequest()->getPost();
			$values['group_id'] = $group_id;
			
			if(empty($values['title']))
				$this->view->error = "Не указан заголовок";
			elseif (!preg_match('/^[a-z]{2}$/si',$values['lang']))
				$this->view->error = "Неправильный формат языка";
			else
			{
				$values['text'] = stripslashes($values['text']);
				$values['type'] = $type;
				if($this->news->add_news($values))
					$this->_redirect('/admin/news');
			}
			$this->view->news = $values;
		}
	}
}
