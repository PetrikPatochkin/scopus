<?php

class Admin_ConfigController extends Zend_Controller_Action
{
	protected $user;
	protected $config_db;
	
	public function init()
	{
		$this->user = new LP_User;
		if (!$this->user->isAdmin()) $this->_redirect('/admin/login');
		$this->_helper->layout->setLayout("admin");
		$this->db_config = new LP_Config;
		
		$this->feedback = new LP_Feedback;
		$this->view->new_feedback = $this->feedback->new_feedback();
	}
	
	public function indexAction()
	{
		
	}
	
	public function defaultmetaAction()
	{
		if ($this->getRequest()->isPost())
		{
			$values=$this->getRequest()->getPost();
			$this->db_config->set_default_meta($values);
		}
		$this->view->values = $this->db_config->default_meta();
	}
	
	public function notificationAction()
	{
	
	
	}
}