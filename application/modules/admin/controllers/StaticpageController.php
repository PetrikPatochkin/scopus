<?php

class Admin_StaticpageController extends Zend_Controller_Action
{
	protected $user;
	protected $stat_page;

    public function init()
    {
		$this->user = new LP_User;
		if (!$this->user->isAdmin()) $this->_redirect('/admin/login');
		$this->_helper->layout->setLayout("admin");
		
		$this->stat_page = new LP_StatPage;
		
		$this->feedback = new LP_Feedback;
		$this->view->new_feedback = $this->feedback->new_feedback();
    }

    public function indexAction()
    {
		$this->view->pages = $this->stat_page->pages();
    }
	
	public function editAction()
	{
		$page_id = $this->getRequest()->getParam('id');
		if (!preg_match('/^\d+$/si',$page_id)) break;
		
		if ($this->getRequest()->isPost())
		{
			$values=$this->getRequest()->getPost();
			$values['text'] = stripslashes($values['text']);
			$this->stat_page->update_page($page_id, $values);
		}
		$this->view->page  = $this->stat_page->page_info($page_id);
			
	}

	public function deleteAction()
	{
		$page_id = $this->getRequest()->getParam('id');
		if (!preg_match('/^\d+$/si',$page_id)) break;
		
		$this->stat_page->delete_page($page_id);
		$this->_redirect('/admin/staticpage');
	}
	
	public function addAction()
	{
		$href = $this->getRequest()->getParam('hr');
		
		if ($href!="") 
		{
			if (!preg_match('/^[a-zA-Z0-9-_]+$/si',$href)) break;
			$this->view->href = $href;
		}
		else
			$this->view->href = false;
		
		if ($this->getRequest()->isPost())
		{
			$values=$this->getRequest()->getPost();
			if ($href!="") $values['href'] = $href;
			if ($values['show']=="on") 
				$values['show']=1;
			else
				$values['show']=0;
			
			if((empty($values['href']))||
				(empty($values['text']))||
				(empty($values['lang'])))
				$this->view->error = "Не заполнены все поля";
			elseif (!preg_match('/^[a-z]{2}$/si',$values['lang']))
				$this->view->error = "Неправильный формат языка";
			elseif($this->stat_page->page_exists($values['href'],$values['lang']))
				$this->view->error = "Страница уже существует";
			else
			{
				$values['text'] = stripslashes($values['text']);
				if($this->stat_page->add_page($values))
					$this->_redirect('/admin/staticpage');
			}
			$this->view->page = $values;
		}
	}
}

