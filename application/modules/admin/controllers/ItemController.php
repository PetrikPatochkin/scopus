<?php

class Admin_ItemController extends Zend_Controller_Action
{
	protected $user;
	protected $catalog;

    public function init()
    {
		$this->user = new LP_User;
		if (!$this->user->isAdmin()) $this->_redirect('/admin/login');
		$this->_helper->layout->setLayout("admin");
		
		$this->catalog = new LP_Catalog;
		
		$this->feedback = new LP_Feedback;
		$this->view->new_feedback = $this->feedback->new_feedback();
    }
	
	public function indexAction()
    {
		if ($_GET['page']) $page = $_GET['page'];
		else 			   $page = 1;
		if ($_GET['search']) $search = $_GET['search'];
		else				 $search = false;
		$limit = 25;
		$this->view->items = $this->catalog->all_items($page,$limit,$search);
		$this->view->search = $search;
		$this->view->cur_page = $page;
		$this->view->page_count = ceil($this->catalog->count_items($search)/$limit);
    }
	
	public function addAction()
	{
		$this->view->categories = $this->catalog->catalog_level2_all();
		
		if ($this->getRequest()->isPost())
		{
			$values = $this->getRequest()->getPost();
			
			if(empty($values['title'])||
			   empty($values['link'])||
			   empty($values['code'])
			)
				$this->view->error = "Не заполнены главные поля";
			else
				{
				$id = $this->catalog->add_item($values);
				$this->redirect("/admin/item/edit?id=".$id);
				}
		}
		
		$this->view->values = $values;
	}
	
	public function editAction()
	{
		if($_GET['id'])	$id=$_GET['id'];
		else			break;
		$this->view->categories = $this->catalog->catalog_level2_all();
		
		
		if ($this->getRequest()->getPost('uploadPhoto'))
		{
			$uploaddir = 'public/images/catalog/';
			$name = md5(time()). "." . end(explode(".", $_FILES['userfile']['name']));
			$uploadfile = $uploaddir . $name;
			if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
				$this->catalog->add_foto($id,$name);
			} else {die("Не вышло");}
		}
		elseif ($this->getRequest()->isPost())
		{
			$values=$this->getRequest()->getPost();
			$this->catalog->update_item($id,$values);
		}
		
		$this->view->link_item = $this->catalog->get_site_link($id);
		$this->view->values = $this->catalog->item_info($id,1,true);
	}
	
	public function deleteAction()
	{
		if($_GET['id'])	$id=$_GET['id'];
		else			break;
		$this->catalog->delete_item($id);
		$this->redirect('/admin/item');
	}
	
	public function delimgAction($id)
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		if($_GET['id']) $item_id = $_GET['id'];
		else break;
		if($_GET['f_id']) $foto_id = $_GET['f_id'];
		else break;
		$this->catalog->del_foto($item_id, $foto_id);
		$this->redirect($this->getRequest()->getHeader('referer'));
	}
}